/*
    Pasteque Android client
    Copyright (C) Pasteque contributors, see the COPYRIGHT file

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package fr.pasteque.client.drivers;

import fr.pasteque.client.Pasteque;
import fr.pasteque.client.drivers.DefaultDeviceManager;
import fr.pasteque.client.drivers.PowaDeviceManager;
import fr.pasteque.client.drivers.POSDeviceManager;
import fr.pasteque.client.drivers.mpop.MPopDeviceManager;
import fr.pasteque.client.drivers.utils.DeviceManagerEventListener;
import fr.pasteque.client.utils.PastequeConfiguration;

public class POSDeviceManagerFactory
{

    /** Create a POSDeviceManager for the main device. Alias of createPosConnection(listener, 0). */
    public static POSDeviceManager createPosConnection(DeviceManagerEventListener listener) {
        return createPosConnection(listener, 0);
    }

    /** Create a POSDeviceManager according to the configuration
     * and set it's listener. */
    public static POSDeviceManager createPosConnection(DeviceManagerEventListener listener, int deviceIndex) {
        POSDeviceManager manager = null;
        switch (Pasteque.getConfiguration().getPrinterDriver(deviceIndex)) {
            case PastequeConfiguration.PrinterDriver.POWAPOS:
                return new PowaDeviceManager(listener);
            case PastequeConfiguration.PrinterDriver.STARMPOP:
                return new MPopDeviceManager(listener);
            default:
                return new DefaultDeviceManager(listener, deviceIndex);
        }
    }

}
